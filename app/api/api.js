'use strict';

var app = angular.module('myApp.api', ['ngFileUpload']);

app.factory('myApi', ['$rootScope', '$http', 'myCookies', 'Upload', '__env', function($rootScope, $http, myCookies, Upload, __env) {

  const urlBase = __env.consultaOnlineApiUrl + '/';

  const urlVideo = __env.agendamentoApiUrl + '/upload/videos/upload';
  const urlImage = __env.agendamentoApiUrl + '/upload/exames_alterados/upload';
  const medMobiUrl = __env.agendamentoApiUrl + '/';

  const staticToken = '6948c21f4fad6e2efc5be95b8a4e4001';

  let token = myCookies.getToken();
  let myApi = {};

  // GET

  myApi.getUser = function(token, userId) {
    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos_prestador/usuario/' + userId + '/' + token,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getProvider = function(id) {
    const query = '/' + id;

    return $http({
      method: 'GET',
      url: medMobiUrl + 'integracao/prestadores' + query,
      headers: {
        'Access-Token': staticToken
      }
    });
  };

  myApi.updateStatusEvento = function(id) {
    // const query = '/' + id;

    return $http({
      method: 'PUT',
      url: medMobiUrl + 'eventos/update-event/'+id,
      data: {'finish':true},
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  };


  myApi.getProviders = function() {
    const query = '/' + token;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos_prestador/prestadores' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getPatient = function(id) {
    const query = '/' + id;

    return $http({
      method: 'GET',
      url: medMobiUrl + 'integracao/pacientes' + query,
      headers: {
        'Access-Token': staticToken
      }
    });
  };

  myApi.getFinishedAppointments = function(providerId) {
    const query = '?filter[where][ativo]=false&filter[where][retorno]=false&filter[where][criador_id]=' + providerId;    
    return $http({
      method: 'GET',
      url: urlBase + 'salas/' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };
  // myApi.getFinishedAppointments = function(providerId) {
  //   const query = '?filter[where][criador_id]=' + providerId;

  //   return $http({
  //     method: 'GET',
  //     url: urlBase + 'salas/consultar_historico_consulta' + query,
  //     headers: {
  //       'X-Access-Token': token
  //     }
  //   });
  // };

  myApi.getFinishedReturns = function(providerId) {
    const query = '?filter[where][ativo]=false&filter[where][retorno]=true&filter[where][criador_id]=' + providerId;    

    return $http({
      method: 'GET',
      url: urlBase + 'salas/' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getPhone = function(id) {
    return $http({
      method: 'GET',
      url: medMobiUrl + 'integracao/pacientes/' + id + '/telefones',
      headers: {
        'Access-Token': staticToken
      }
    });
  };

  myApi.getRoom = function(userId, roomId) {
    token = myCookies.getToken();

    const query = roomId ? '/' + roomId : '?filter={"where":{"and":[{"criador_id":"' + userId + '"},{"ativo": "true"}]}}';

    return $http({
      method: 'GET',
      url: urlBase + 'salas' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getRecords = function(id) {
    const query = '/' + id + '/' + token;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos_prestador/prontuarios' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getRecordsExams = function(id) {
    const query = '/' + id + '/' + token;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos_prestador/prontuarios_exames' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getAppointmentReceipts = function(id) {
    const query = '/' + id;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos/atendimento_receituarios' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getAppointmentReferrals = function(id) {
    const query = '/' + id;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos/atendimento_encaminhamentos' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getExams = function(id) {
    const query = id + '/exames' + '?access_token=' + token;

    return $http({
      method: 'GET',
      url: medMobiUrl + 'pacientes/' + query,
      crossDomain: true,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getParticipant = function(roomId, participantId) {
    const query = '?filter={"where":{"and":[{"sala_id":"' + roomId + '"},{"usuario_id":' + participantId + '},{"ativo":"true"}]}}';

    return $http({
      method: 'GET',
      url: urlBase + 'participantes' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getMessages = function(roomId) {
    const query = '?filter[where][sala_id]=' + roomId;

    return $http({
      method: 'GET',
      url: urlBase + 'mensagens' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getMessageControl = function(messageId) {
    const query = '?filter[where][mensagem_id]=' + messageId;

    return $http({
      method: 'GET',
      url: urlBase + 'controles_mensagens' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getRoomVideos = function(roomId) {
    const query = '?filter[where][sala_id]=' + roomId;

    return $http({
      method: 'GET',
      url: urlBase + 'videos' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getVideos = function(id) {
    const query = '/' + id;

    return $http({
      method: 'GET',
      url: urlBase + 'videos' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.downloadVideo = function(id){
    const query = `/${id}.webm/${token}`;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos_prestador/download_video' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getEditedExams = function(roomId) {
    const query = '?filter[where][sala_id]=' + roomId;

    return $http({
      method: 'GET',
      url: urlBase + 'imagens' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getDocuments = function(id) {
    const query = `/${id}/${token}`;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos_prestador/documentos_medicos' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.getDigitalExam = function(id) {
    // const query = '/' + id;
    // const query = '?filter[where][pac_id]=' + id;
    const query = `/${id}/${token}`;

    return $http({
      method: 'GET',
      url: urlBase + 'apicronicos_prestador/exames_digitais' + query,
      headers: {
        'X-Access-Token': token
      }
    });
  };

  // POST

  myApi.login = function(email, password) {
    var auth = "email=" + email + "&password=" + password + "&grant_type=thePassword";

    return $http({
      method: "POST",
      url: urlBase + 'apicronicos/login',
      data: auth,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
  };

  myApi.resetPassword = function(email, url) {
    // var data = "email=" + email + "&url=" + url;

    var data = {
      "email": email,
      "url": url
    }

    return $http({
      method: "POST",
      url: urlBase + 'apicronicos/reset_senha',
      data: data,
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };

  myApi.redefinePassword = function(token, passwordConfirmation, password) {
    var data = {
      "confirmation": passwordConfirmation,
      "password": password, // ID do usuário
      "accessToken": token
    }

    return $http({
      method: "POST",
      url: urlBase + 'apicronicos/redefinir_senha',
      data: data,
      headers: {
        'Content-Type': 'application/json'
      }
    });
  };

  myApi.sendInvite = function(roomId, participantId, participantEmail) {
    var data = {
      "sala_id": roomId,
      "participante_id": participantId, // ID do usuário
      "participante_email": participantEmail
    }

    return $http({
      method: "POST",
      url: urlBase + 'salas/convidar',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  }

  myApi.logConsulta = function(title, type, url,prestador,paciente,data_consulta) {


    var _url = url.split(/\//g)
    var lastPath = _url.pop()
    url = _url.join('/') + '/' + paciente + '/'+ lastPath

    var data = {
      "title":title,
      "type":type,
      "url":url,
      "id_prestador":prestador,
      "id_paciente":paciente,
      "data_consulta":data_consulta
    }
    
    return $http({
      method: "POST",
      url: medMobiUrl + 'logconsultas',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  }

  myApi.sendNotification = function(roomId, patientId, patientEmail) {
    var data = {
      "sala_id": roomId,
      "paciente_id": patientId,
      "paciente_email": patientEmail
    }

    return $http({
      method: "POST",
      url: urlBase + 'salas/notificar',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  };

  myApi.sendMessage = function(roomId, msg) {
    var dt = new Date().toISOString();

    var data = {
      "sala_id": roomId,
      "remetente_id": $rootScope.usuario.id,
      "data_hora_envio": dt,
      "prestador": $rootScope.provider,
      "mensagem": msg
    }

    return $http({
      method: "POST",
      url: urlBase + 'mensagens',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  };

  myApi.sendMessageStatus = function(roomId, msgId, receiverId, status) {
    var dt = new Date().toISOString();

    var data = {
      "sala_id": roomId,
      "mensagem_id": msgId,
      "destinatario_id": receiverId,
      "acao": status,
      "data_hora": dt
    }

    return $http({
      method: "POST",
      url: urlBase + 'controles_mensagens',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  };

  myApi.sendParticipant = function(roomId, participantId) {
    var dt = new Date().toISOString();

    var data = {
      "sala_id": roomId,
      "usuario_id": participantId,
      "createdAt": dt
    };

    console.log(data);

    return $http({
      method: "POST",
      url: urlBase + 'participantes',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  };

  myApi.sendParticipantVideo = function(roomId, participantId, video) {
    var dt = new Date().toISOString();

    var data = {
      "descricao": "\"Vídeo sem descrição\"",
      "file_id": video.name,
      "container_id": "video",
      "sala_id": roomId,
      "createdAt": dt
    };

    // Envia o registro do vídeo para a API da Consulta Online
    $http({
      method: "POST",
      url: urlBase + 'videos',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    }).error(
      function(error) {
        console.log(error);
    });


    // Faz upload do arquivo do vídeo
    return Upload.upload({
      url: urlVideo,
      data: {
        file: video
      },
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.sendVideo = function(roomId, video) {
    var dt = new Date().toISOString();

    var data = {
      "descricao": "\"Vídeo sem descrição\"",
      "file_id": video.name,
      "container_id": "video",
      "sala_id": roomId,
      "createdAt": dt
    };

    $http({
      method: "POST",
      url: urlBase + 'videos',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });

    return Upload.upload({
      url: urlVideo,
      data: {
        file: video
      },
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.sendImageRegister = function(roomId, image) {
    var dt = new Date().toISOString();

    var data = {
      "descricao": "\"Imagem sem descrição\"",
      "file_id": image.name,
      "container_id": "exames_alterados",
      "sala_id": roomId,
      "createdat": dt
    };

    return $http({
      method: "POST",
      url: urlBase + 'imagens',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    }).error(function(error) {
      console.log(error);
    });
  };

  myApi.sendImage = function(image) {
    return Upload.upload({
      url: urlImage,
      data: {
        file: image
      },
      headers: {
        'X-Access-Token': token
      }
    });
  };

  myApi.closeRoom = function(roomId) {
    var data = {
      "sala_id": roomId
    }

    return $http({
      method: "POST",
      url: urlBase + 'salas/encerrar',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  };

  myApi.removeParticipant = function(userId, roomId) {
    var data = {
      "usuario": userId,
      "sala": roomId
    }

    return $http({
      method: "POST",
      url: urlBase + 'participantes/remover',
      data: data,
      headers: {
        'X-Access-Token': token,
        'Content-Type': 'application/json'
      }
    });
  };

  return myApi;
}]);
