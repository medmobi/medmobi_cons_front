'use strict';

var app = angular.module('myApp.retornoNovo', []);

app.controller('RetornoNovoCtrl', ['$scope', '$rootScope', '$routeParams', '$window', '$location', 'myCookies', 'myApi', function($scope, $rootScope, $routeParams, $window, $location, myCookies, myApi) {

  var roomId = $routeParams.room; // Busca ID da sala na URL

  $scope.paciente = [];
  $scope.exames = [];
  $scope.examesPdf = [];

  // Busca informações da sala atual
  myApi.getRoom(null, roomId).then(
    function(response) {
      if (!response.data.ativo) {
        _block('Esse retorno já foi finalizado', '/retorno');
      } else {
        var idUser = response.data.paciente_id; // User ID
        $scope.criador = response.data.criador_id;

        // Busca paciente da sala
        myApi.getPatient(idUser).then(
          function(patient) {
            const avatar = patient.data.pacientes[0].foto ? $rootScope.photo + patient.data.pacientes[0].foto : '../img/avatar.png';
            const idPatient = patient.data.pacientes[0].id;
            let date = new Date(patient.data.pacientes[0].data_nascimento);

            date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

            $scope.paciente = {
              id: patient.data.id,
              nome: patient.data.nome,
              // Caso o paciente não tenha e-mail, dispara retorno para o e-mail do Edivaldo
              email: patient.data.email,
              sexo: patient.data.pacientes[0].sexo.toUpperCase(),
              data_nasc: date,
              avatar: avatar
            };

            $scope.paciente.idApi = idPatient;

            myApi.getPhone(idPatient).then(
              function(patient) {
                $scope.paciente.telefone = patient.data && patient.data[0] && patient.data[0][0] ? patient.data[0][0].numero : 'Não possui';
              },
              function(error) {
                console.log(error);
              });

            _getPatientExams(patient.data.pacientes[0].id);

          },
          function(error) {
            console.log(error);
          });
      }
    },
    function(error) {
      console.log(error);
    });

  function _getPatientExams(id) {
    var data = {};

    myApi.getExams(id).then(
      function(exams) {
        exams.data.forEach(function(exam) {
          data = {
            "id": exam.id,
            "titulo": exam.titulo,
            "imagem": $rootScope.exam + exam.arquivo
          };

          var format = exam.arquivo.split('.');

          if (format[format.length - 1] === 'pdf') {
            $scope.examesPdf.push(data);
          } else {
            $scope.exames.push(data);
          }
        });

        // Aplica primeira imagem no Canvas
        $scope.make_base();

        // Inicia o Slick Slider
        $scope.slickConfig = {
          dots: true,
          enabled: true,
          draggable: true,
          variableWidth: true,
          centerMode: true,
          focusOnSelect: true,
          method: {},
          event: {
            beforeChange: function(event, slick, currentSlide, nextSlide) {},
            afterChange: function(event, slick, currentSlide, nextSlide) {}
          }
        };
      },
      function(error) {
        console.log(error);
      });
  };

  // Canvas

  var canvas = document.getElementById('can'),
    ctx = canvas.getContext("2d"),
    w = canvas.width,
    h = canvas.height,
    base_image,
    flag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0,
    dot_flag = false,
    x = "red",
    y = 4,
    font = '18px sans-serif',
    hasInput = false;

  $scope.text = false;

  canvas.addEventListener("mousemove", function(e) {
    _findxy('move', e)
  }, false);

  canvas.addEventListener("mousedown", function(e) {
    if ($scope.text) {
      if (hasInput) return;
      _addInput(e);
    } else {
      _findxy('down', e);
    }
  }, false);

  canvas.addEventListener("mouseup", function(e) {
    _findxy('up', e);
  }, false);

  canvas.addEventListener("mouseout", function(e) {
    _findxy('out', e);
  }, false);

  canvas.addEventListener("touchstart", function(e) {
    _findxy('down', e)
  }, false);

  canvas.addEventListener("touchmove", function(e) {
    _findxy('move', e);
    e.preventDefault();
  }, false);

  function _addInput(e) {
    var clientX = e.clientX,
      clientY = e.clientY,
      input = document.createElement('input');

    input.type = 'text';
    input.id = 'textTool';
    input.style.position = 'fixed';
    input.style.left = (clientX - 4) + 'px';
    input.style.top = (clientY - 4) + 'px';
    input.style.color = x;
    input.style.border = 'none';
    input.style.borderradius = '3px';
    input.style.width = '600px';
    input.style.outline = 'none';
    input.style.background = 'none';

    input.onkeydown = _handleEnter;
    input.addEventListener("focusout", _handleEnter);

    document.body.appendChild(input);

    setTimeout(function() {
      $('#textTool').focus();
    }, 0);

    hasInput = true;
  };

  function _handleEnter(e) {
    if (e) {
      if (e.keyCode != 13 && e.type != 'focusout') return;
    }

    _drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));

    $('#textTool').blur();
    $('#textTool').remove();

    hasInput = false;
  };

  function _drawText(txt, clientX, clientY) {
    var rect = canvas.getBoundingClientRect(), // Pega tamanho visual do canvas
      scaleX = canvas.width / rect.width, // Pega altura real do canvas
      scaleY = canvas.height / rect.height; // Pega largura real do canvas

    currX = (clientX - rect.left) * scaleX;
    currY = (clientY - rect.top) * scaleY;

    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.fillStyle = x;
    ctx.font = font;
    ctx.fillText(txt, currX, currY);
  };

  $scope.make_base = function(src) {
    var img = new Image();

    img.crossOrigin = '';

    img.src = src ? src : '../img/noexam.jpg';

    if (src) $scope.loading = true;

    $scope.lastSrc = img.src;

    img.onload = function() {
      var hRatio = w / img.width;
      var vRatio = h / img.height;
      var ratio = Math.min(hRatio, vRatio);
      var centerShift_x = (w - img.width * ratio) / 2;
      var centerShift_y = (h - img.height * ratio) / 2;
      ctx.clearRect(0, 0, w, h);
      ctx.drawImage(img, 0, 0, img.width, img.height, centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);

      $scope.loading = false;
      $scope.$apply();
    }

  };

  $scope.pencils = [4, 9, 14];
  $scope.colors = ['red', 'green', 'blue', 'yellow', 'orange', 'black', 'white'];

  var colorOld,
    colorNew = 'red',
    pencilOld,
    pencilNew = 'thickness-4';

  $scope.changeThickness = function(val, e) {
    pencilOld = pencilNew;
    pencilNew = e.target.id;

    // Remove borda da cor antiga e adiciona na nova
    $('#' + pencilOld).removeClass('selectedTool');
    $('#' + pencilNew).addClass('selectedTool');

    // Muda a cor para a selecionada
    y = val;
  };

  $scope.changeColor = function(e) {
    colorOld = colorNew;
    colorNew = e.target.id;

    // Remove borda da cor antiga e adiciona na nova
    $('#' + colorOld).removeClass('selectedTool');
    $('#' + colorNew).addClass('selectedTool');

    // Muda a cor para a selecionada
    x = colorNew;
  };

  function _draw() {
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    ctx.strokeStyle = x;
    ctx.lineWidth = y;
    ctx.stroke();
    ctx.closePath();
  };

  function _findxy(res, e) {
    var rect = canvas.getBoundingClientRect(),
      scaleX = canvas.width / rect.width,
      scaleY = canvas.height / rect.height;

    if (res == 'down') {
      prevX = currX;
      prevY = currY;
      currX = (e.clientX - rect.left) * scaleX;
      currY = (e.clientY - rect.top) * scaleY;

      flag = true;
      dot_flag = true;
      if (dot_flag) {
        ctx.beginPath();
        ctx.fillStyle = x;
        ctx.fillRect(currX, currY, 2, 2);
        ctx.closePath();
        dot_flag = false;
      }
    }

    if (res == 'up' || res == "out") {
      flag = false;
    }

    var clientX, clientY;

    if (e.type === "touchmove") {
      clientX = e.touches[0].clientX;
      clientY = e.touches[0].clientY;
    } else {
      clientX = e.clientX;
      clientY = e.clientY;
    }

    if (res == 'move') {
      if (flag) {
        prevX = currX;
        prevY = currY;
        currX = (clientX - rect.left) * scaleX;
        currY = (clientY - rect.top) * scaleY;
        _draw();
      }
    }
  };

  var recorder,
    videoElement = document.querySelector('video'),
    fileExtension = 'webm',
    fileName = _getFileName(),
    mimeType = 'video/webm',
    blob;

  function _captureScreen(cb) {
    getScreenId(function(error, sourceId, screen_constraints) {

      var videoConstraints = {
          audio: false,
          video: {
              mandatory: {
                chromeMediaSource: screen_constraints.video.mandatory.chromeMediaSource,
                chromeMediaSourceId: screen_constraints.video.mandatory.chromeMediaSourceId,
                maxWidth:1280,
                maxHeight:760,
                maxFrameRate: 20
              }
          }
      };
      navigator.mediaDevices.getUserMedia(videoConstraints).then(cb).catch(function(error) {
        console.error('getScreenId error', error);

        alert('Falha ao capturar sua tela. Verifique se o plugin foi instalado corretamente e se você permitiu o compartilhamento de tela.');
      });
    });
  };

  function _captureCamera(cb) {
    navigator.mediaDevices.getUserMedia({
      audio: true,
      video: true
    }).then(cb).catch(cb(false));
  };

  function _keepStreamActive(stream) {
    videoElement.muted = true;
    setSrcObject(stream, videoElement);
    (document.body || document.documentElement).appendChild(videoElement);
  };

  function _getRandomString() {
    if (window.crypto && window.crypto.getRandomValues && navigator.userAgent.indexOf('Safari') === -1) {
      var a = window.crypto.getRandomValues(new Uint32Array(3)),
        token = '';
      for (var i = 0, l = a.length; i < l; i++) {
        token += a[i].toString(36);
      }
      return token;
    } else {
      return (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
    }
  };

  function _getFileName(fileExtension) {
    var d = new Date();
    var year = d.getUTCFullYear();
    var month = d.getUTCMonth();
    var date = d.getUTCDate();
    return 'RecordRTC-' + year + month + date + '-' + _getRandomString() + '.' + fileExtension;
  };

  $scope._end = _end

  function _end() {
    myApi.closeRoom(roomId).then(
      function(response) {  
        // debugger;      
        myApi.logConsulta('Realizado um retorno de uma consulta','Retorno',$location.$$absUrl,$scope.criador,$scope.paciente.id)
        window.onbeforeunload = '';
        $location.path('/retorno');
      },
      function(error) {
        if (error.status === 401) $rootScope.logOut();

        console.log(error);
      });
  };

  function _block(msg, url) {
    if (msg) alert(msg);
    event.preventDefault();
    $location.path(url);
    return;
  };

  $scope.startRecording = function() {
    _captureScreen(function(screen) {
      _keepStreamActive(screen);
      _captureCamera(function(camera) {
        screen.width = window.screen.width;
        screen.height = window.screen.height;
        screen.fullcanvas = true;

        if (camera) {
          _keepStreamActive(camera);

          camera.width = 320;
          camera.height = 240;
          camera.top = screen.height - camera.height;
          camera.left = screen.width - camera.width;

          recorder = RecordRTC([screen, camera], {
            type: 'video',
            mimeType: mimeType,
            previewStream: function(s) {
              videoElement.muted = true;
              setSrcObject(camera, videoElement);
            }
          });
        } else {
          recorder = RecordRTC([screen], {
            type: 'video',
            mimeType: mimeType
          });
        }


        $(videoElement).show();

        recorder.startRecording();

        $scope.stopRecording = function() {
          recorder.stopRecording(function() {
            blob = recorder.getBlob();

            $('#preview').attr('src', URL.createObjectURL(blob));

            $('#btn-send-recording').html('Enviar');
            $('#btn-send-recording').show();
            $('#cancel').show();

            $('.progress').hide();
            $('.progress .progress-bar').css('width', '0%');
            $('.progress .progress-bar').html('0%');

            $("#modalPreview").modal();

            $(videoElement).hide();

            if (camera) {
              [screen, camera].forEach(function(stream) {
                stream.getVideoTracks().forEach(function(track) {
                  track.stop();
                });

                stream.getAudioTracks().forEach(function(track) {
                  track.stop();
                });
              });
            } else {
              [screen].forEach(function(stream) {
                stream.getVideoTracks().forEach(function(track) {
                  track.stop();
                });

                stream.getAudioTracks().forEach(function(track) {
                  track.stop();
                });
              });
            }
          });

          $('#btn-start-recording').html('<i class="fa fa-refresh">&nbsp;</i> Gravar novamente');
          $scope.iCanEnd = true
          $('#btn-start-recording').show();
          $('#btn-stop-recording').hide();
        };
      });

      $('#btn-start-recording').hide();
      $('#btn-stop-recording').show();
    });
  };

  $scope.sendRecording = function() {
    // Lógica para upload ao servidor
    var file = new File([blob], roomId + '.webm', {
      type: blob.type
    });

    // Lógica para envio de vídeo para servidor
    myApi.sendVideo(roomId, file).then(
      function(response) {
        myApi.sendNotification(roomId, $scope.paciente.id, $scope.paciente.email).then(
          function(response) {
            var url = 'https://' + window.location.host + '/#/retorno/' + $scope.paciente.id + '/' + roomId;
            $('.progress .progress-bar').html('O vídeo foi enviado com sucesso! <a href="' + url + '" target="_blank">Clique aqui para visualizar.</a>');

            // Encerra retorno e redireciona
            setTimeout(function(){
              _end();
            }, 4000);
          },
          function(error) {
            alert('Ocorreu um erro ao disparar o e-mail para o paciente');
            console.log(error);
          });

        recorder.destroy();
        recorder = null;
      },
      function(error) {
        alert('Ocorreu um erro ao enviar seu vídeo');

        $('.progress').hide();
        $('.progress .progress-bar').css('width', '0%');
        $('.progress .progress-bar').html('0%');

        $('#btn-send-recording').html('<i class="fa fa-refresh">&nbsp;</i> Tentar novamente');
        $('#btn-send-recording').show();
        $('#cancel').show();
        console.log(error);
      },
      function(progress) {
        var percent = parseInt((progress.loaded / progress.total) * 100);
        $('.progress .progress-bar').css('width', percent + '%');
        $('.progress .progress-bar').html(percent + '%');
      }
    );

    $('.progress').show();
    $('#btn-send-recording').hide();
    $('#cancel').hide();
  };

  $scope.$on('$routeChangeStart', function($event, next, current) {
    // Ao usuário navegar a outra página, recarrega a página para evitar duplicidade de vídeos
    if (next.$$route.originalPath.indexOf('/login') != 0) {
      setTimeout(function () {
        $window.location.reload();
      }, 500);
    }
  });
}]);
