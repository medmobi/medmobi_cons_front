'use strict';

var app = angular.module('myApp.sala', ['ngRoute', 'slickCarousel', 'angularMoment']);

app.controller('SalaCtrl', ['$scope', '$rootScope', '$routeParams', '$window', '$location', 'myApi', 'myCookies', '__env', function($scope, $rootScope, $routeParams, $window, $location, myApi, myCookies, __env) {

  const roomId = $routeParams.room; // Busca ID da sala na URL
  const token = myCookies.getToken();

  var isRecording = false;

  $scope.exames = [];
  $scope.examesPdf = [];

  $scope.prestadores = [];
  $scope.selecionados = [];
  $scope.mensagens = [];
  $scope.visualizacao = "exame";
  $scope.chat = false;
  $scope.loading = false;
  $scope.chatLoader = true;

  function sortByProperty(property) {
    return function(x, y) {
      return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
    };
  };

  // Busca as mensagens da API e atribui elas ao Scope
  myApi.getMessages(roomId).then(
    function(response) {
      let mensagens = [];
      let itemsProcessed = 0;
      if (response.data.length > 0) {
        response.data.forEach(function(msg) {
          let mensagem = new Object();
          const data = new Date(msg.data_hora_envio);
          const items = response.data.length;

          mensagem.id = msg.id;
          mensagem.local = msg.remetente_id === $rootScope.usuario.id;
          mensagem.chatMessage = false;
          mensagem.mensagem = msg.mensagem;
          mensagem.hora = data.getHours() + ":" + (data.getMinutes() < 10 ? '0' : '') + data.getMinutes();
          mensagem.avatar = '../img/avatar.png';

          myApi.getMessageControl(msg.id).then(
            function(control) {
              if (control.data.length > 0) {
                mensagem.status = control.data[control.data.length - 1].acao === 1 ? 'received' : 'read';
              } else {
                mensagem.status = 'sent';
              }
            },
            function(error) {
              if (error.status === 401) $rootScope.logOut(roomId);

              console.log(error);
            });

          if (msg.prestador) {
            myApi.getProvider(msg.remetente_id).then(
                function(response) {
                  console.log(response);
                  mensagem.nome = response.data[0].nome;
                  mensagem.avatar = '../img/avatarMedico.png';

                  mensagens.push(mensagem);

                  itemsProcessed++;

                  if (itemsProcessed == items) {
                    mensagens.sort(sortByProperty('id'));
                    _pullMessages(mensagens);
                  }
                },
                function(error) {
                  if (error.status === 401) $rootScope.logOut(roomId);

                  console.log(error);
                });
          } else {
            myApi.getPatient(msg.remetente_id).then(
                function(patient) {
                  mensagem.nome = patient.data.nome ? patient.data.nome : 'Anônimo';
                  mensagem.avatar = patient.data.foto ? $rootScope.photo + patient.data.foto : '../img/avatar.png';

                  mensagens.push(mensagem);

                  itemsProcessed++;

                  if (itemsProcessed == items) {
                    mensagens.sort(sortByProperty('id'));
                    _pullMessages(mensagens);
                  }
                },
                function(error) {
                  if (error.status === 401) $rootScope.logOut(roomId);

                  console.log(error);
                });
          }
        });
      } else {
        $scope.chatLoader = false;
      }
    },
    function(error) {
      if (error.status === 401) $rootScope.logOut(roomId);

      console.log(error);
    }
  );

  if ($rootScope.provider) {
    myApi.getProviders().then(
        function(response) {
          _filterProviders(response);
        },
        function(error) {
          if (error.status === 401) $rootScope.logOut(roomId);

          console.log(error);
        }
    );
  }

  $scope.convidar = function() {
    // myApi.sendEmail($scope.selecionados);
    $scope.success = false;

    // Adiciona participantes selecionados no banco de dados
    $scope.selecionados.forEach(function(participant) {
      myApi.sendParticipant(roomId, participant.id).then(
        function(response) {
          // Após adicionar os participantes dispara o e-mail

          console.log('SEND PARTICIPANT');
          console.log(response);
          myApi.sendInvite(roomId, participant.id, participant.email).then(
            function(response) {
              console.log('SEND INVITE');
              console.log(response);
              _removeProviders(participant);
            },
            function(error) {
              if (error.status === 401) $rootScope.logOut(roomId);

              console.log(error);
            });
        },
        function(error) {
          if (error.status === 401) $rootScope.logOut(roomId);

          alert('Ocorreu um erro ao convidar o participante ' + participant.nome);
          console.log(error);
        });
    });
  };

  $scope.toggleSelection = function toggleSelection(name) {
    var idx = $scope.selecionados.indexOf(name);

    if (idx > -1) {
      $scope.selecionados.splice(idx, 1);
    } else {
      $scope.selecionados.push(name);
    }
  };

  $scope.encerrar = function() {
    if (isRecording) {
      var sendRecordings = confirm('Deseja salvar as gravações de vídeo no servidor?');

      if (sendRecordings) {
        // Para a gravação e envia para o servidor
        _stopRecording(_sendVideo);
      } else {
        _end();
      }
    } else {
      _end();
    }
  };

  $scope.online = [];
  $scope.paciente = [];
  $scope.criador = '';

  // Busca informações da sala atual
  myApi.getRoom(null, roomId).then(
    function(response) {
      let idUser = response.data.paciente_id;

      $scope.criador = response.data.criador_id;
      $scope.evento_id = response.data.evento_id;      
      $scope.owner = $scope.criador === $rootScope.usuario.id;

      // Busca paciente da sala
      myApi.getPatient(idUser).then(
        function(patient) {
          let idPatient = patient.data.pacientes[0].id;

          let avatar = patient.data.foto ? $rootScope.photo + patient.data.foto : '../img/avatar.png',
            date = new Date(patient.data.pacientes[0].data_nascimento);
              // date = new Date();

          if ($scope.owner || patient.data.id === $rootScope.usuario.id) {
            connection.openOrJoin(roomId);

            var i = 0;

            $scope.$watch('chat', function(e) {
              if (i = 1) {
                connection.extra.chat = e;
                connection.updateExtraData();
                connection.send({
                  chat: true
                });
              }

              i = 1;
            });
          } else {
              myApi.getParticipant(roomId, $rootScope.usuario.id).then(
              function(response) {
                if (response.data.length > 0) {
                  connection.openOrJoin(roomId);

                  var i = 0;

                  $scope.$watch('chat', function(e) {
                    if (i = 1) {
                      connection.extra.chat = e;
                      connection.updateExtraData();
                      connection.send({
                        chat: true
                      });
                    }

                    i = 1;
                  });
                }
              },
              function(error) {
                if (error.status === 401) $rootScope.logOut(roomId);

                console.log(error);
              });
          }

          date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

          $scope.paciente = {
            id: patient.data.id,
            nome: patient.data.nome,
            sexo: patient.data.pacientes[0].sexo.toUpperCase(),
            data_nasc: date,
            avatar: avatar
          };

          $scope.paciente.idApi = patient.data.id; // Id do usuário

          myApi.getPhone(idPatient).then(
            function(patient) {
              // $scope.paciente.celular = patient.data[0] ? patient.data[0].numero : 'Não possui';
              $scope.paciente.telefone = patient.data && patient.data[0] && patient.data[0][0] ? patient.data[0][0].numero : 'Não possui';
            },
            function(error) {
              if (error.status === 401) $rootScope.logOut(roomId);

              console.log(error);
            });

            // Busca exames e prontuário
            if ($rootScope.provider) {
              _getPatientRecords(idPatient);
              _getPatientExams(idPatient);
            }
        },
        function(error) {
          if (error.status === 401) $rootScope.logOut(roomId);

          console.log(error);
        });

      // _getPatientExams(idPatient);
    },
    function(error) {
      if (error.status === 401) $rootScope.logOut(roomId);

      console.log(error);
    });

  $scope.prontuario = [];

  function _pullMessages(messages) {
    $scope.chatLoader = false;
    $scope.mensagens = messages;

    setTimeout(function() {
      $('.post-comments').scrollTop(100000000000000000);
    }, 0);
  };

  function _getPatientRecords(id) {
    myApi.getRecords(id).then(
      function(records) {
        for (let i = 0; i < records.data.length; i++) {
          if (records.data[i].receituarios.length > 0) records.data[i].label = 'receituario';
          if (records.data[i].encaminhamentos.length > 0) records.data[i].label = 'encaminhamentos';

          $scope.prontuario.push(records.data[i]);
        }
      },
      function(error) {
        if (error.status === 401) $rootScope.logOut(roomId);

        console.log(error);
      });

      myApi.getRecordsExams(id).then(
        function(response){
          if (response.data.length > 0) {
            for (let i = 0; i < response.data.length; i++) {
              response.data[i].label = 'arquivos';
              response.data[i].inicio = response.data[i].createdAt;
              $scope.prontuario.push(response.data[i]);
            }
          }
        },
        function(error){
          if (error.status === 401) $rootScope.logOut(roomId);

          console.log(error);
        });

        myApi.getDocuments(id).then(
          function(response){
            if (response.data.length > 0) {
              for (var i = 0; i < response.data.length; i++) {
                response.data[i].label = 'documentos';
                response.data[i].inicio = response.data[i].createdAt;
                $scope.prontuario.push(response.data[i]);
              }
            }
          },
          function(error){
            if (error.status === 401) $rootScope.logOut(roomId);

            console.log(error);
          });

        myApi.getDigitalExam(id).then(
          function(response){
            if (response.data.length > 0) {
              for (var i = 0; i < response.data.length; i++) {
                response.data[i].label = 'exames';
                response.data[i].inicio = response.data[i].createdAt;
                $scope.prontuario.push(response.data[i]);
              }
            }
          },
          function(error){
            if (error.status === 401) $rootScope.logOut(roomId);

            console.log(error);
          });
  };

  function _filterProviders(providers) {
    providers.data.forEach(function(provider) {
      myApi.getParticipant(roomId, provider.id).then(
        function(response) {
          // Lista caso o usuário participante ainda não tenha sido convidado, e não seja o mesmo usuário logado
          if (response.data.length === 0 && provider.id != $rootScope.usuario.id) $scope.prestadores.push(provider);
        },
        function(error) {
          if (error.status === 401) $rootScope.logOut(roomId);

          console.log(error);
        });
    });
  };

  function _removeProviders(provider) {
    const idx = $scope.prestadores.indexOf(provider);

    $scope.prestadores.splice(idx, 1);
  };

  function _getPatientExams(id) {
    let data = {};

    myApi.getExams(id).then(
      function(exams) {
        exams.data.forEach(function(exam) {
          data = {
            "id": exam.id,
            "titulo": exam.titulo,
            "imagem": $rootScope.exam + exam.arquivo
          };

          const format = exam.arquivo.split('.');

          // Verifica se arquivo existe antes de buscar
          if (format[format.length - 1] === 'pdf') {
            $scope.examesPdf.push(data);
          } else {
            $scope.exames.push(data);
          }

        });

        // Aplica primeira imagem no Canvas
        $scope.make_base();

        // Inicia o Slick Slider
        $scope.slickConfig = {
          dots: true,
          enabled: true,
          draggable: true,
          variableWidth: true,
          centerMode: true,
          focusOnSelect: true,
          method: {},
          event: {
            beforeChange: function(event, slick, currentSlide, nextSlide) {},
            afterChange: function(event, slick, currentSlide, nextSlide) {}
          }
        };
      },
      function(error) {
        if (error.status === 401) $rootScope.logOut(roomId);

        console.log(error);
      });
  };

  function _startRTC() {
    var idApi = $rootScope.usuario.id,
      username = $rootScope.usuario.nome || 'Anônimo';

    connection.extra = {
      idApi: idApi,
      username: username,
      avatar: $rootScope.usuario.avatar,
      video: true,
      audio: true,
      chat: $scope.chat
    };
  };

  function _userOnline(info) {
    var user = {
      "id": info.userid,
      "idApi": info.extra.idApi,
      "avatar": info.extra.avatar,
      "nome": info.extra.username,
      "chat": info.extra.chat
    };

    _receivedMessages();

    $scope.online.push(user);
    $scope.$apply();
  };

  function _receivedMessages(changeChat) {
    // Caso o usuário seja prestador, verifica se o paciente está online e vice-versa
    var receiver = $rootScope.provider ? $scope.paciente.idApi : $scope.criador;

    // Verifica se o usuário receptor da mensagem (paciente ou criador da sala) não está online
    if (!_checkIfReceiverIsOnline(receiver)) return; // Se o usuário não estiver online, corta a execução

    // Recebe as mensagens caso o usuário esteja online
    for (var i = 0; i < $scope.mensagens.length; i++) {
      if ($scope.mensagens[i].local && !$scope.mensagens[i].chatMessage) {
        if ($scope.mensagens[i].status === 'sent') {
          // Envia para a API que a mensagem foi lida
          myApi.sendMessageStatus(roomId, $scope.mensagens[i].id, receiver, 1).error(function(error) {
            if (error.status === 401) $rootScope.logOut(roomId);

            console.log(error);
          });
          $scope.mensagens[i].status = 'received';
        }
      }
    }

    if (!_checkIfChatIsOpen(receiver, changeChat)) return; // Se o chat não estiver aberto, corta a execução

    for (var i = 0; i < $scope.mensagens.length; i++) {
      if ($scope.mensagens[i].local && !$scope.mensagens[i].chatMessage) {
        if ($scope.mensagens[i].status === 'received') {
          myApi.sendMessageStatus(roomId, $scope.mensagens[i].id, receiver, 2).error(function(error) {
            if (error.status === 401) $rootScope.logOut(roomId);

            console.log(error);
          });
          $scope.mensagens[i].status = 'read';
        }
      }
    }
  };

  function _checkIfReceiverIsOnline(receiver) {
    var on = false;

    connection.getAllParticipants().forEach(function(participantId) {
      // Busca todos os peers e verifica se o paciente ou criador está online
      var peer = connection.peers[participantId];

      if (peer.extra.idApi === receiver) {
        on = true;
      }
    });

    return on;
  };

  function _checkIfChatIsOpen(receiver, changeChat) {
    // Caso o usuário seja prestador, verifica se o paciente está online e vice-versa
    var open = false;

    connection.getAllParticipants().forEach(function(participantId) {
      // Busca todos os peers e verifica se o paciente ou criador está online e se o chat está aberto
      var peer = connection.peers[participantId];

      if (typeof changeChat !== 'undefined') {
        // Verifica se o peer analisado é o usuário que mudou o chat, e se esse usuário é criador/paciente da sala
        if (peer.userid === changeChat.userid && peer.extra.idApi === receiver) {
          // Caso o usuário seja válido e seja o criador/paciente da sala, inverte a ação do changeChat
          open = !changeChat.extra.chat;

        }
      } else {
        // Caso a variável changeChat não esteja definida
        if (peer.extra.idApi === receiver && peer.extra.chat) {
          // Verifica se o usuário é criador/paciente da sala e se o chat dele está aberto
          open = true;
        }
      }
    });

    return open;
  };

  function _userOffline(id) {
    var data = $.grep($scope.online, function(e) {
      return e.id != id;
    });

    $scope.online = data;

    $scope.$apply();
  };

  function _addNewMessage(args) {
    var mensagem = new Object();
    var data = new Date();

    mensagem.id = args.msgId ? args.msgId : false;
    mensagem.local = args.local;
    mensagem.chatMessage = args.chatMessage;
    mensagem.nome = args.header;
    mensagem.mensagem = args.message;
    mensagem.avatar = args.avatar;
    mensagem.hora = data.getHours() + ":" + (data.getMinutes() < 10 ? '0' : '') + data.getMinutes();
    mensagem.status = 'sent';

    $scope.mensagens.push(mensagem);

    _receivedMessages();

    setTimeout(function() {
      $scope.$apply();
      $('.post-comments').scrollTop(100000000000000000);
    }, 0);

    if (args.callback) {
      args.callback(args.avatar);
    }
  };

  function _insertMedia(e) {
    var media = e.stream.isVideo ? ' video' : ' audio';

    if ($scope.owner) _addRecording(e);

    // Detecta se já foi inserido alguma midia de usuário, para evitar vídeos duplicados
    if ($('#' + e.userid + media).length > 0) return;

    e.mediaElement.controls = false;

    var element = e.type === 'remote' ? e.userid : 'media-local';

    if (e.stream.isVideo) {
      $('#' + element + ' img').hide();
      if (e.type === 'local') $('#desativar-video').show();
    }

    if (e.type === 'local') $('#desativar-audio').show();

    $('#' + element).prepend(e.mediaElement);

    // Detecta se as mídias estão ativas, caso não estejam, muta a mídia novamente para os usuários que acessarem posteriormente
    if (!e.extra.video) _hideMedia({
      "type": "remote",
      "muteType": "video",
      "userid": e.userid
    });
  };

  var recorder = new Object(),
    video = document.querySelector('video');

  var records = 0;

  function _addRecording(e){
    recorder[records] = RecordRTC(e.stream, {
      type: 'video'
    });

    recorder[records].camera = e.stream;
    recorder[records].idApi = e.extra.idApi;
    recorder[records].username = e.extra.username;

    if (records === 0 && $scope.owner) $('#gravar').show();

    records += 1;
  }

  $scope.iniciarGravacao = function() {
    for (var i = 0; i < records; i++) {
      recorder[i].startRecording();
    }

    isRecording = true;

    if (records === 0 && $scope.owner) {
      window.onbeforeunload = function() {
        return "Você tem certeza que deseja sair? Você perderá todas as gravações de vídeo";
      }
    }

    $('#gravar').html('Gravando...').attr('disabled', true);
    $('#encerrar').html('Encerrar e enviar gravação');
  };

  var idx = 0;

  function _stopRecording(callback) {
    if (records > 0) {
      if (idx < records) {
        callback(_stopRecording); // _sendVideo
      } else {
        $('.progress .progress-bar').html('Sala encerrada com sucesso!');
        recorder = null;

        // Encerra sala
        _end();
      }
    } else {
      // Encerra a sala
      _end();
    }
  };



  function _end() {
    
    myApi.closeRoom(roomId).then(
      function(response) {
        connection.send({
          closedRoom: true
        });
        // debugger;      
        myApi.logConsulta('Realizado uma consulta online','Consulta',$location.$$absUrl,$scope.criador,$scope.paciente.id)
        myApi.updateStatusEvento($scope.evento_id)
        window.onbeforeunload = '';
        $location.path('/inicio');
      },
      function(error) {
        if (error.status === 401) $rootScope.logOut(roomId);

        console.log(error);
      });
  };

  function _sendVideo(callback) {
    recorder[idx].stopRecording(function() {
      var blob = recorder[idx].getBlob();
      var stream = connection.streamEvents[recorder[idx].camera.streamid];
      var idApi = recorder[idx].idApi;
      var name = recorder[idx].username;

      var file = new File([blob], roomId + "-" + idApi + '.webm', {
        type: blob.type
      });

      // Envia vídeo do dono, paciente e participantes da sala
      myApi.sendParticipantVideo(roomId, idApi, file).then(
        function(response) {
          // Quando o vídeo é enviado com sucesso
          if (response.status === 200) {
            recorder[idx].destroy();

            idx++;

            callback(_sendVideo);
          }

        },
        function(error) {
          alert('Ocorreu um erro ao enviar o vídeo do ' + name + ' para o servidor');

          $('.progress').hide();
          $('.progress .progress-bar').css('width', '0%');
          $('.progress .progress-bar').html('0%');

          $('#encerrar').html('<i class="fa fa-refresh">&nbsp;</i> Tentar novamente');
          $('#encerrar').show();

          if (error.status === 401) $rootScope.logOut(roomId);

          console.log(error);
        },
        function(progress) {
          $('#convidar').hide();
          $('#encerrar').hide();
          $('#gravar').hide();
          $('.progress').show();

          var percent = parseInt((progress.loaded / progress.total) * 100);
          $('.progress .progress-bar').css('width', percent + '%');
          $('.progress .progress-bar').html((idx + 1) + ' de ' + records + ' - ' + percent + '%');
        });
    });
  };

  function _hideMedia(e) {
    var element = e.type === "remote" ? e.userid : 'media-local';

    $('#' + element + ' img').show();
    $('#' + element + ' ' + e.muteType).hide();
  };

  function _showMedia(e) {
    $('#media-local video').prop('muted', true);

    var element = e.type === "remote" ? e.userid : 'media-local';

    $('#' + element + ' img').hide();
    $('#' + element + ' ' + e.unmuteType).show();
  };

  // Canvas

  var canvas,
    ctx,
    w,
    h,
    base_image,
    flag = false,
    prevX = 0,
    currX = 0,
    prevY = 0,
    currY = 0,
    dot_flag = false,
    x = "red",
    y = 4,
    font = '18px sans-serif',
    hasInput = false;

  $scope.text = false;

  function _startCanvas() {
    canvas = document.getElementById('can');
    ctx = canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;

    canvas.addEventListener("mousemove", function(e) {
      _findxy('move', e)
    }, false);

    canvas.addEventListener("mousedown", function(e) {
      if ($scope.text) {
        if (hasInput) return;
        _addInput(e);
      } else {
        _findxy('down', e);
      }
    }, false);

    canvas.addEventListener("mouseup", function(e) {
      _findxy('up', e);
      _save();
    }, false);

    canvas.addEventListener("mouseout", function(e) {
      _findxy('out', e);
    }, false);

    canvas.addEventListener("touchstart", function(e) {
      _findxy('down', e)
    }, false);

    canvas.addEventListener("touchmove", function(e) {
      _findxy('move', e);
      e.preventDefault();
    }, false);

    canvas.addEventListener("touchend", function(e) {
      _save();
    }, false);
  };


  function _addInput(e) {
    var clientX = e.clientX,
      clientY = e.clientY,
      input = document.createElement('input');

    input.type = 'text';
    input.id = 'textTool';
    input.style.position = 'fixed';
    input.style.left = (clientX - 4) + 'px';
    input.style.top = (clientY - 4) + 'px';
    input.style.color = x;
    input.style.border = 'none';
    input.style.borderradius = '3px';
    input.style.width = '600px';
    input.style.outline = 'none';
    input.style.background = 'none';

    input.onkeydown = _handleEnter;
    input.addEventListener("focusout", _handleEnter);

    document.body.appendChild(input);

    setTimeout(function() {
      $('#textTool').focus();
    }, 0);

    hasInput = true;
  };

  function _handleEnter(e) {
    if (e) {
      if (e.keyCode != 13 && e.type != 'focusout') return;
    }

    _drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));

    $('#textTool').blur();
    $('#textTool').remove();

    hasInput = false;

    _save();
  };

  function _drawText(txt, clientX, clientY) {
    var rect = canvas.getBoundingClientRect(), // Pega tamanho visual do canvas
      scaleX = canvas.width / rect.width, // Pega altura real do canvas
      scaleY = canvas.height / rect.height; // Pega largura real do canvas


    currX = (clientX - rect.left) * scaleX;
    currY = (clientY - rect.top) * scaleY;

    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';
    ctx.fillStyle = x;
    ctx.font = font;
    ctx.fillText(txt, currX, currY);
  };

  $scope.make_base = function(src) {
    var img = new Image();

    img.crossOrigin = '';

    img.src = src ? src : '../img/noexam.jpg';

    if (src) $scope.loading = true;

    $scope.lastSrc = img.src;

    img.onload = function() {
      var hRatio = w / img.width;
      var vRatio = h / img.height;
      var ratio = Math.min(hRatio, vRatio);
      var centerShift_x = (w - img.width * ratio) / 2;
      var centerShift_y = (h - img.height * ratio) / 2;
      ctx.clearRect(0, 0, w, h);
      ctx.drawImage(img, 0, 0, img.width, img.height, centerShift_x, centerShift_y, img.width * ratio, img.height * ratio);

      if (src) _save();

      $scope.loading = false;
      $scope.$apply();
    }

  };

  $scope.pencils = [4, 9, 14];
  $scope.colors = ['red', 'green', 'blue', 'yellow', 'orange', 'black', 'white'];

  var colorOld,
    colorNew = 'red',
    pencilOld,
    pencilNew = 'thickness-4';

  $scope.changeThickness = function(val, e) {
    pencilOld = pencilNew;
    pencilNew = e.target.id;

    // Remove borda da cor antiga e adiciona na nova
    $('#' + pencilOld).removeClass('selectedTool');
    $('#' + pencilNew).addClass('selectedTool');

    // Muda a cor para a selecionada
    y = val;
  };

  $scope.changeColor = function(e) {
    colorOld = colorNew;
    colorNew = e.target.id;

    // Remove borda da cor antiga e adiciona na nova
    $('#' + colorOld).removeClass('selectedTool');
    $('#' + colorNew).addClass('selectedTool');

    // Muda a cor para a selecionada
    x = colorNew;
  };

  function _draw() {
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    ctx.strokeStyle = x;
    ctx.lineWidth = y;
    ctx.stroke();
    ctx.closePath();
  };

  function _save() {
    var mensagem = {
      img: true,
      base64: canvas.toDataURL()
    };

    _sendExam(mensagem);
  };

  var exam = [];

  function _sendExam(mensagem){
    var imageInfo = $scope.lastSrc.split('/');
    imageInfo = imageInfo[imageInfo.length - 1].split('.');

    if (imageInfo[0] !== 'noexam') {
      var blob = _dataURItoBlob(mensagem.base64);

      var file = new File([blob], roomId + "-" + imageInfo[0] + '.' + imageInfo[1], {
        type: 'image/' + imageInfo[1]
      });

      exam[imageInfo[0]] = exam[imageInfo[0]] >= 0 ? exam[imageInfo[0]] + 1 : 0;

      if (exam[imageInfo[0]] === 1) {
        myApi.sendImageRegister(roomId, file);
      }

      if (exam[imageInfo[0]] > 0) myApi.sendImage(file);

      connection.send(mensagem);
    }
  };

  function _dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {
      type: mimeString
    });
  };

  function _findxy(res, e) {
    var rect = canvas.getBoundingClientRect(),
      scaleX = canvas.width / rect.width,
      scaleY = canvas.height / rect.height;

    if (res == 'down') {
      prevX = currX;
      prevY = currY;
      currX = (e.clientX - rect.left) * scaleX;
      currY = (e.clientY - rect.top) * scaleY;

      flag = true;
      dot_flag = true;
      if (dot_flag) {
        ctx.beginPath();
        ctx.fillStyle = x;
        ctx.fillRect(currX, currY, 2, 2);
        ctx.closePath();
        dot_flag = false;
      }
    }

    if (res == 'up' || res == "out") {
      flag = false;
    }

    var clientX, clientY;

    if (e.type === "touchmove") {
      clientX = e.touches[0].clientX;
      clientY = e.touches[0].clientY;
    } else {
      clientX = e.clientX;
      clientY = e.clientY;
    }

    if (res == 'move') {
      if (flag) {
        prevX = currX;
        prevY = currY;
        currX = (clientX - rect.left) * scaleX;
        currY = (clientY - rect.top) * scaleY;
        _draw();
      }
    }
  };

  var connection = new RTCMultiConnection();

  connection.socketURL = __env.rtcUrl + '/';
  // connection.socketURL = 'https://rtcmulticonnection.herokuapp.com/';

  connection.session = {
    data: true,
    video: true,
    audio: true
  };

  // connection.maxParticipantsAllowed = 20;
  connection.autoCloseEntireSession = false;
  connection.closeBeforeUnload = false;
  // connection.session.broadcast = true;

  connection.onopen = function(e) {
    _addNewMessage({
      idApi: e.extra.idApi,
      header: 'Novo usuário',
      local: true,
      message: e.extra.username + ' acessou a sala...',
      avatar: '../img/avatar.png',
      chatMessage: true
    });

    _userOnline(e);
  };

  $scope.liveExam = '../img/noexam.jpg';

  connection.onmessage = function(e) {
    if (e.data.userRemoved === true) {
      if (e.data.removedUserId == connection.userid) {

        myApi.removeParticipant($rootScope.usuario.id, roomId).then(
          function(response) {
            connection.close();
            connection.closeSocket();

            var url = $rootScope.provider ? '#/inicio' : '#/login';

            $window.location.href = url;
          },
          function(error) {
            if (error.status === 401) $rootScope.logOut(roomId);

            console.log(error);
          });
      }
    }

    if (e.data.closedRoom === true) {
      connection.close();
      connection.closeSocket();

      var url = $rootScope.provider ? '#/inicio' : '#/login';

      $window.location.href = url;
    }

    if (e.data.chat) {
      _receivedMessages(e);
      return;
    }
    if (e.data.img) {
      $scope.liveExam = e.data.base64;
      $scope.$apply();
      return;
    }

    _addNewMessage({
      idApi: e.extra.idApi,
      header: e.extra.username,
      local: false,
      message: e.data,
      avatar: e.extra.avatar
    });

    // document.title = e.data;

    document.querySelector('#message-sound').play();
  };

  connection.onstream = function(e) {
    _insertMedia(e);
  };

  connection.sendMessage = function(message) {
    message.userid = connection.userid;
    message.extra = connection.extra;
    connection.sendCustomMessage(message);
  };

  connection.onclose = function(e) {
    _addNewMessage({
      header: e.extra.username,
      local: true,
      message: e.extra.username + ' saiu da sala...',
      avatar: '../img/avatar.png',
      chatMessage: true
    });

    _userOffline(e.userid);
  };

  connection.onleave = function(e) {
    _userOffline(e.userid);
  };

  connection.onmute = function(e) {
    if (e.muteType === "video") {
      connection.extra.video = e.type === "local" ? false : connection.extra.video;

      _hideMedia(e);
    } else {
      connection.extra.audio = e.type === "local" ? false : connection.extra.audio;
    }

    connection.updateExtraData();
  };

  connection.onunmute = function(e) {
    if (e.unmuteType === "video") {
      connection.extra.video = e.type === "local" ? true : connection.extra.video;

      _showMedia(e);
    } else {
      connection.extra.audio = e.type === "local" ? true : connection.extra.audio;
    }

    connection.updateExtraData();
  };

  connection.onMediaError = function(e) {
    // Quando o usuário não habilita nenhuma media (Microfone e Câmera)
    connection.session = {
      data: true
    };

    connection.openOrJoin(roomId);
  };

  // Inicia todos os serviços
  $scope.init = function() {
    _startRTC();
    _startCanvas();
  };

  $scope.mutar = function(mute, media, element) {
    if (mute) {
      connection.attachStreams.forEach(function(stream) {
        stream.mute(media);
      });

      if (element) $('#ativar-' + media).show();
    } else {
      connection.attachStreams.forEach(function(stream) {
        stream.unmute(media);
      });
      if (element) $('#desativar-' + media).show();
    }

    if (element) $(element.currentTarget).hide();
  };

  // Mensagem
  var isShiftKeyPressed = false;
  $('#mensagem').keydown(function(e) {
    if (e.keyCode == 16) {
      isShiftKeyPressed = true;
    }
  });

  $('#mensagem').keyup(function(e) {

    if (isShiftKeyPressed) {
      if (e.keyCode == 16) {
        isShiftKeyPressed = false;
      }
      return;
    }

    if (e.keyCode != 13) return;

    var mensagem = this.value.trim();

    if (mensagem) {
      myApi.sendMessage(roomId, mensagem).then(
        function(msg) {
          _addNewMessage({
            msgId: msg.data.id,
            header: connection.extra.username,
            local: true,
            message: mensagem,
            avatar: connection.extra.avatar
          });
        },
        function(error) {
          if (error.status === 401) $rootScope.logOut(roomId);

          alert('Ocorreu um erro ao enviar sua mensagem');
          console.log(error);
        });

      connection.send(mensagem);
    }

    this.value = '';
  });

  $('#enviar-mensagem').click(function() {
    var mensagem = $('#mensagem').val().trim();


    if (mensagem) {
      myApi.sendMessage(roomId, mensagem).then(
        function(msg) {
          _addNewMessage({
            msgId: msg.data.id,
            header: connection.extra.username,
            local: true,
            message: mensagem,
            avatar: connection.extra.avatar
          });
        },
        function(error) {
          if (error.status === 401) $rootScope.logOut(roomId);

          alert('Ocorreu um erro ao enviar sua mensagem');
          console.log(error);
        });

      connection.send(mensagem);
    }

    $('#mensagem').val('');
    $('#mensagem').focus();
  });

  $scope.$on('$routeChangeStart', function($event, next, current) {
    // Ao usuário navegar a outra página, recarrega a página para evitar duplicidade de vídeos
    if (next.$$route.originalPath.indexOf('/login') != 0) {
      setTimeout(function () {
        $window.location.reload();
      }, 500);
    }
  });

  $scope.removeParticipant = function(userId) {
    // if (!connection.isInitiator) return;

    connection.send({
      userRemoved: true,
      removedUserId: userId
    });
  };
}]);
