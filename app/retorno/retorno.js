'use strict';

var app = angular.module('myApp.retorno', ['ngRoute']);

app.filter('startFrom', function(){
  return function(data, start){
    return data.slice(start);
  }
});

app.controller('RetornoCtrl', ['$scope', '$location', '$rootScope', '$window', 'myCookies', 'myApi', 'uibPaginationConfig', function($scope, $location, $rootScope, $window, myCookies, myApi, uibPaginationConfig) {
  uibPaginationConfig.nextText='Próxima';
  uibPaginationConfig.previousText='Anterior';
  uibPaginationConfig.firstText="«";
  uibPaginationConfig.lastText="»";

  $scope.title = "Retorno";
  $scope.prestador = myCookies.getUserName();
  $scope.loading = true;
  $scope.pacientes = [];
  $scope.historicos = [];
  $scope.currentPage = 1;
  $scope.HcurrentPage = 1; // Página inicial do histórico
  $scope.pageSize = 10; // Quantos resultados vai trazer na paginação

  $scope.$watch('searchText', function(){
    $scope.currentPage = 1;
    $scope.HcurrentPage = 1;
  });

  var userId = myCookies.getUserId();

  $scope.sendEmail = function(roomId, patientId, patientEmail){
    $('#enviarEmail').removeClass('btn-primary').addClass('btn-default');
    $('#enviarEmail').html('Enviando');

    myApi.sendNotification(roomId, patientId, patientEmail).then(
      function(response) {
        $('#enviarEmail').removeClass('btn-default').addClass('btn-success');
        $('#enviarEmail').html('<i class="fa fa-check"></i> Enviado');

        setTimeout(function(){
          $('#enviarEmail').removeClass('btn-success').addClass('btn-primary');
          $('#enviarEmail').html('Enviar e-mail');
        }, 4000);
      },
      function(error) {
        alert('Ocorreu um erro ao disparar o e-mail para o paciente');
        console.log(error);
      });
  }

  var histories = 0;

  myApi.getRoom(userId).then(
    function(response) {

      // Busca todas as salas do criador logado
      response.data.forEach(function(room) {
        if (room.retorno) {
          // Para cada sala, busca e lista seu paciente
          myApi.getPatient(room.paciente_id).then(
            function(response){
              console.log(response);
              _listReturns(room, response);
            },
            function(error) {
              if (error.status === 401) $rootScope.logOut();

              console.log(error);
            });
          }
        });

        $scope.loading = false;
    },
    function(error) {
      if (error.status === 401) $rootScope.logOut();

      console.log(error);
    });

    myApi.getFinishedReturns(userId).then(
      function(response) {
        var i = 0;

        // $.each(response.data, function(index, value){
          response.data.forEach(function(room) {
            // Para cada retorno, busca e lista seu paciente
            myApi.getPatient(room.paciente_id).then(
              function(response){
                _listHistory(room, response);
              },
              function(error) {
                if (error.status === 401) $rootScope.logOut();

                console.log(error);
              });

              i++;
          });
        // });
        if (i === 0) $('.list-group-lg').append('<p class="ml-lg mb-md" style="opacity: 0.5">Nenhum agendamento encontrado</p>');

        $scope.loading = false;
      },
      function(error) {
        if (error.status === 401) $rootScope.logOut();

        console.log(error);
      });

    function _listReturns(room, patient) {
      if (patient.data) {
        const avatar = patient.data.pacientes[0].foto ? $rootScope.photo + patient.data.pacientes[0].foto : '../img/avatar.png';
        const date = new Date(room.data_prevista);

        $scope.pacientes.push({
          id: patient.data.id,
          nome: patient.data.nome,
          sala: room.id,
          data_prevista: date,
          avatar: avatar
        });
      }
    };

    function _listHistory(room, patient) {
      if (patient.data) {
        const avatar = patient.data.pacientes[0].foto ? $rootScope.photo + patient.data.pacientes[0].foto : '../img/avatar.png';
        const date = new Date(room.data_prevista);
        const dateSent = new Date(room.data_envio_email || room.updatedat);
        
        $scope.historicos.push({
          id: patient.data.id,
          nome: patient.data.nome,
          email: patient.data.email,
          sala: room.id,
          data_prevista: date,
          data_enviada: dateSent,
          avatar: avatar
        });
      }
    };
}]);
