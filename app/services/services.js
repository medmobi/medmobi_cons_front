'use strict';

var app = angular.module('myApp.services', []);

app.service('SessionService', ['myCookies', function(myCookies) {
  var userIsAuthenticated = false;
  var userIsProvider = false;

  this.getUserAuthenticated = function() {
    userIsAuthenticated = myCookies.getToken() ? true : false;

    return userIsAuthenticated;
  };

  this.getUserPermission = function() {
    userIsProvider = myCookies.getProvider() === 'true' ? true : false;

    return userIsProvider;
  };

}]);
