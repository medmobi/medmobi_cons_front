'use strict';

var app = angular.module('myApp.cookies', ['ngCookies']);

app.factory('myCookies', ['$cookies', function($cookies) {
  var profileData = {};

  profileData.setCookieData = function(token, userId, patientId, userName, userEmail, avatar, provider) {
    $cookies.put("token", token);
    $cookies.put("userId", userId);
    $cookies.put("patientId", patientId);
    $cookies.put("userName", userName);
    $cookies.put("userEmail", userEmail);
    $cookies.put("userAvatar", avatar);
    $cookies.put("provider", provider);

    return true;
  };

  profileData.getToken = function() {
    return $cookies.get("token");
  };

  profileData.getUserName = function() {
    return $cookies.get("userName");
  };

  profileData.getUserId = function() {
    return $cookies.get("userId");
  };

  profileData.getPatientId = function() {
    return $cookies.get("patientId");
  };

  profileData.getUserEmail = function() {
    return $cookies.get("userEmail");
  };

  profileData.getUserAvatar = function() {
    return $cookies.get("userAvatar");
  };

  profileData.getProvider = function() {
    return $cookies.get("provider");
  };

  profileData.remove = function() {
    $cookies.remove("token");
    $cookies.remove("userId");
    $cookies.remove("patientId");
    $cookies.remove("userName");
    $cookies.remove("userEmail");
    $cookies.remove("userAvatar");
    $cookies.remove("provider");
  };

  return profileData;
}]);
