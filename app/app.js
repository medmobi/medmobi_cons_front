'use strict';

var app = angular.module('myApp', [
  'ngRoute',
  'ngCookies',
  'ui.bootstrap',
  'myApp.services',
  'myApp.login',
  'myApp.inicio',
  'myApp.sala',
  'myApp.salaVisualizar',
  'myApp.retorno',
  'myApp.retornoNovo',
  'myApp.retornoVisualizar',
  'myApp.esqueciSenha',
  'myApp.redefinirSenha',
  'myApp.cookies',
  'myApp.api'
]);

var env = {};
if(window){
  Object.assign(env, window.__env);
}
app.constant('__env', env);

app.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $routeProvider.
  when('/login', {
      templateUrl: 'login/login.html',
      controller: 'LoginCtrl',
      requireLogin: false,
      requireProvider: false
    })
    .when('/login/:room', {
      templateUrl: 'login/login.html',
      controller: 'LoginCtrl',
      requireLogin: false,
      requireProvider: false
    })
    .when('/login/:id/:video', {
      templateUrl: 'login/login.html',
      controller: 'LoginCtrl',
      requireLogin: false,
      requireProvider: false
    })
    .when('/inicio', {
      templateUrl: 'inicio/inicio.html',
      controller: 'InicioCtrl',
      requireLogin: true,
      requireProvider: true
    })
    .when('/sala/:room', {
      templateUrl: 'sala/sala.html',
      controller: 'SalaCtrl',
      requireLogin: true,
      requireProvider: true
    })
    .when('/sala/:id/:room', {
      templateUrl: 'salaVisualizar/salaVisualizar.html',
      controller: 'SalaVisualizarCtrl',
      requireLogin: true,
      requireProvider: true
    })
    .when('/retorno', {
      templateUrl: 'retorno/retorno.html',
      controller: 'RetornoCtrl',
      requireLogin: true,
      requireProvider: true
    })
    .when('/retorno/:room', {
      templateUrl: 'retornoNovo/retornoNovo.html',
      controller: 'RetornoNovoCtrl',
      requireLogin: true,
      requireProvider: true
    })
    .when('/retorno/:id/:video', {
      templateUrl: 'retornoVisualizar/retornoVisualizar.html',
      controller: 'RetornoVisualizarCtrl',
      requireLogin: true,
      requireProvider: true
    })
    .when('/esqueci', {
      templateUrl: 'esqueciSenha/esqueciSenha.html',
      controller: 'EsqueciSenhaCtrl',
      requireLogin: false,
      requireProvider: false
    })
    .when('/nova-senha', {
      templateUrl: 'redefinirSenha/redefinirSenha.html',
      controller: 'RedefinirSenhaCtrl',
      requireLogin: false,
      requireProvider: false
    })
    .otherwise({
      redirectTo: '/login'
    });
}]);

app.run(function($rootScope, $route, $window, $location, $routeParams, SessionService, myApi, myCookies, __env) {
  // Inicia variáveis globais
  $rootScope.err = false;
  $rootScope.navbarHide = true;
  $rootScope.isRoom = false;
  $rootScope.usuario = [];
  $rootScope.photo = __env.agendamentoApiUrl + '/upload/fotos/download/';
  $rootScope.exam = __env.agendamentoApiUrl + '/upload/exames/download/';
  $rootScope.rtcUrl = __env.rtcUrl;

  // Verifica se o usuário está logado
  var my_access = SessionService.getUserAuthenticated();

  if (my_access) {
    // Se estiver logado, verifica a permissão, atribui os valores e busca os avatares
    $rootScope.usuario.id = Number(myCookies.getUserId());
    $rootScope.usuario.idPaciente = Number(myCookies.getPatientId());
    $rootScope.usuario.nome = myCookies.getUserName();
    $rootScope.usuario.email = myCookies.getUserEmail();
    $rootScope.usuario.avatar = myCookies.getUserAvatar();
    $rootScope.provider = SessionService.getUserPermission();
  }

  $rootScope.logOut = function(roomId) {
    myCookies.remove();
    $window.location.reload();
    $window.location.href = roomId ? '#/login/' + roomId : '#/login';
  };

  var routes = $route.routes;

  $rootScope.$on('$locationChangeStart', function(event, next) {

    // Atualiza os valores, caso o login tenha sido alterado
    my_access = SessionService.getUserAuthenticated();

    $rootScope.usuario.id = Number(myCookies.getUserId());
    $rootScope.usuario.idPaciente = Number(myCookies.getPatientId());
    $rootScope.usuario.nome = myCookies.getUserName();
    $rootScope.usuario.email = myCookies.getUserEmail();
    $rootScope.usuario.avatar = myCookies.getUserAvatar();
    $rootScope.provider = SessionService.getUserPermission();

    next = next.split("/");

    // Verifica se é login para ocultar a navbar
    if (next[4]) { // Verifica se a próxima página não é a página inicial
      if (next[4] === 'sala' || next[4] === 'retorno') {
        // Cria URL customizada para login de acordo com a sala que o usuário está tentando acessar
        $rootScope.isRoom = next[4] === 'sala' && !next[6];

        var returnView = next[6] && next[4] === 'retorno' ? next[6] : false; // Se for uma visualização de retorno, atribui o id
        var roomView = next[6] && next[4] === 'sala' ? next[6] : false; // Se for uma visualização de sala, atribui o id
        var room = next[5] ? next[5] : false;
        var url = '/login';

        if (room) url = returnView ? url + '/' + room + '/' + returnView : url + '/' + room;

        var roomId = room;

        if (returnView) {
          roomId = returnView;
        } else if (roomView) {
          roomId = roomView;
        }

        if (roomId) {
          // Busca sala
          myApi.getRoom(null, roomId).then(
            function(room) {
              if (room.data.ativo || !room.data.ativo && room.data.retorno || !room.data.ativo && roomView){
                if (room.data.retorno && !returnView) url = '/login'; // Remove o ID da URl caso seja um novo retorno, para não gerar conflito ao recuperar url

                if (my_access) { // Verifica se está logado
                  if ((room.data.retorno && next[4] === 'sala') || (!room.data.retorno && next[4] === 'retorno')) {
                    // Caso o usuário acesse sala ou retorno com link incorreto
                    _block('Link de ' + next[4] + ' inválido!', url);
                  } else {
                    if ($rootScope.provider) { // Verifica se é um Prestador
                      if ($rootScope.usuario.id != room.data.criador_id) { // Verifica se não é o criador da sala

                        myApi.getParticipant(roomId, $rootScope.usuario.id).then(
                          function(response) {
                            if (response.data.length === 0) { // Verifica se foi convidado para sala
                              _block('Você não tem permissão para acessar essa página', url);
                            }
                          },
                          function(error) {
                            console.log(error);
                          }
                        );
                      }
                    } else { // Usuário é um paciente
                      if (room.data.retorno && !returnView) { // Caso seja um retorno, mas não seja a visualização
                        _block('Você não tem permissão para acessar essa página', url);
                      } else {
                        if ($rootScope.usuario.idPaciente != room.data.paciente_id) { // Verifica se é um paciente dessa sala
                          _block('Você não tem permissão para acessar essa página', url);
                        }
                      }
                    }
                  }
                } else {
                  if (room.data.retorno && !returnView) url = '/login';
                  _block('Você precisa estar logado para acessar essa página', url);
                }
              } else {
                _block('A sala não existe ou foi encerrada', '/login');
              }
            },
            function(error) {
              if (error.status === 401) {
                _block('Você precisa estar logado para acessar essa página', url);
              } else {
                _block('A sala/retorno não existe ou foi encerrada(o)', '/login');
              }

              console.log(error);
            });
        } else {
          _validate(url);
        }

      } else {
        var url = '/login';
        _validate(url);
      }

      $rootScope.navbarHide = next === 'login';
    } else {
      // Oculta a navbar caso seja a página inicial
      $rootScope.navbarHide = true;
    }


    function _validate(url) {
      var route = '';
      var requireLogin = true;

      next = next[4];

      for (var k in routes) {
        if (routes[k].regexp) {
          route = routes[k].regexp.toString().replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');

          if (route === next) {
            if (routes[k].requireLogin) {
              if (my_access) { // Verifica se está autenticado
                if (routes[k].requireProvider) { // Verifica se a página requer permissão de prestador
                  if (!$rootScope.provider) { // Verifica se o usuário é não prestador
                    _block('Você não tem permissão para acessar essa página', url);
                  }
                }
              } else {
                _block('Você precisa logar para acessar essa página', url);
                break;
              }
            } else { // Qualquer um pode acessar
              break;
            }
            break;
          }
        } else {
          break;
        }
      }
    };

    function _block(msg, url) {
      if (msg) $rootScope.err = msg;
      $location.path(url);
      event.preventDefault();
      return;
    };
  });

});
