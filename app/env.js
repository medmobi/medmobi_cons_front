(function (window) {
    window.__env = window.__env || {};

    // API url
    window.__env.consultaOnlineWebUrl = 'https://aol.medmobi.tk';
    window.__env.consultaOnlineApiUrl = 'https://aol.medmobi.tk/api-atendimento';


    window.__env.agendamentoWebUrl = 'https://app.medmobi.tk';
    window.__env.agendamentoApiUrl = 'https://app.medmobi.tk/api';
    
    // window.__env.consultaOnlineWebUrl = 'http://localhost:8000';
    // window.__env.consultaOnlineApiUrl = 'http://localhost:3001/api-atendimento';


    // window.__env.agendamentoWebUrl = 'http://localhost:8080';
    // window.__env.agendamentoApiUrl = 'http://localhost:3000/api';

    window.__env.rtcUrl = 'https://rtc.medmobi.tk';

}(this));