'use strict';

var app = angular.module('myApp.login', ['ngRoute']);

app.controller('LoginCtrl', ['$rootScope', '$scope', '$routeParams', '$window', '$location', 'myApi', 'myCookies', function($rootScope, $scope, $routeParams, $window, $location, myApi, myCookies) {
  if ($rootScope.err) {
    $scope.error = $rootScope.err;

    $rootScope.err = false;
  } else {
    $scope.error = false;
  }

  $scope.login = function() {
    $scope.loading = true;

    myApi.login($scope.email, $scope.password).then(
      function(response) {
        _authenticationSuccess(response);
      },
      function(error) {
        _authenticationError(error);
      }
    );
  };

  function _authenticationSuccess(response) {
    $scope.loading = false;

    const token = response.data.id;
    const userId = response.data.userId;
    const idPatient = response.data.userId;
    const name = response.data.user.nome;
    const email = response.data.user.email;
    const provider = response.data.user.pres_id !== null;

    $scope.setCookies = function(avatar){
      myCookies.setCookieData(token, userId, idPatient, name, email, avatar, provider);

      _redirect(provider);
    }

    if (!provider) {
      myApi.getPatient(idPatient, token).then(
        function(patient) {
          const avatar = patient.data.foto ? $rootScope.photo + patient.data.foto : '../img/avatar.png';

          // const avatar = '../img/avatar.png';

          //Adiciona informações no cookie do navegador
          $scope.setCookies(avatar);
        },
        function(error) {
          console.log(error);
        });
    } else {
      //Adiciona informações no cookie do navegador
      $scope.setCookies('../img/avatarMedico.png');
    }
  };

  function _authenticationError(error) {
    $scope.loading = false;

    alert("Login e/ou senha inválidos");

    console.log(error);
  };

  function _redirect(provider) {
    var urlType, url;

    if ($routeParams.room) {
      url = '#/sala/' + $routeParams.room;
    } else if ($routeParams.id && $routeParams.video) {
      url = '#/retorno/' + $routeParams.id + '/' + $routeParams.video;
    } else {
      url = '#/inicio';
    }

    if (!provider && !$routeParams.room && !$routeParams.id && !$routeParams.video) {
      $scope.error = 'Você logou com sucesso, mas precisa de um link de sala/retorno válido para acessar';
      return;
    }

    $window.location.href = url;
  };
}]);
