'use strict';

var app = angular.module('myApp.redefinirSenha', ['ngRoute']);

app.controller('RedefinirSenhaCtrl', ['$rootScope', '$scope', '$routeParams', '$window', '$location', 'myApi', 'myCookies', function($rootScope, $scope, $routeParams, $window, $location, myApi, myCookies) {
  if ($rootScope.err) {
    $scope.error = $rootScope.err;

    $rootScope.err = false;
  } else {
    $scope.error = false;
  }

  $scope.loading = false;

  $scope.redefine = function() {
    const password = $scope.password;
    const passwordConfirmation = $scope.passwordConfirmation;

    if (password && passwordConfirmation) {
      if (password === passwordConfirmation) {
        let url_string = window.location.href
        let url = new URL(url_string);
        console.log(url);
        if (url.hash) {
          const token = url.hash.split('access_token=');

          if (token[1]) {
            $scope.loading = true;
            myApi.redefinePassword(token[1], passwordConfirmation, password).then(
              function(response) {
                console.log(response);
                _displaySuccess('Sua senha foi alterada com sucesso!');
              },
              function(error) {
                console.log(error);
                _displayError('Ocorreu um erro no servidor, tente novamente mais tarde');
              }
            );
          } else {
            _displayError('Token expirado, faça outra solicitação de recuperação de senha');
          }
        }
      } else {
        _displayError('As senhas não coincidem');
      }
    } else {
      _displayError('Por favor, preencham os campos corretamente');
    }
  };

  function _displaySuccess(msg) {
    $scope.loading = false;
    delete $scope.error;
    $scope.success = msg;
    $('.widget-login').hide();
    return;
  };

  function _displayError(msg) {
    $scope.loading = false;
    delete $scope.success;
    $scope.error = msg;
    return;
  };
}]);
