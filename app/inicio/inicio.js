'use strict';

var app = angular.module('myApp.inicio', ['ngRoute']);

app.filter('startFrom', function(){
  return function(data, start){
    return data.slice(start);
  }
});

app.controller('InicioCtrl', ['$scope', '$rootScope', '$window', '$location', '$http', 'myCookies', 'myApi', 'uibPaginationConfig', function($scope, $rootScope, $window, $location, $http, myCookies, myApi, uibPaginationConfig) {
  uibPaginationConfig.nextText='Próxima';
  uibPaginationConfig.previousText='Anterior';
  uibPaginationConfig.firstText="«";
  uibPaginationConfig.lastText="»";

  $scope.title = "Consulta On-line";
  $scope.prestador = myCookies.getUserName();
  $scope.loading = true;
  $scope.loadingH = true;
  $scope.pacientes = [];
  $scope.historicos = [];
  $scope.currentPage = 1;
  $scope.HcurrentPage = 1; // Página inicial do histórico
  $scope.pageSize = 10; // Quantos resultados vai trazer na paginação

  $scope.$watch('searchText', function(){
    $scope.currentPage = 1;
    $scope.HcurrentPage = 1;
  });

  const userId = myCookies.getUserId();

  myApi.getRoom(userId).then(
    function(response) {
      let i = 0;
      // Busca todas as salas do criador logado
      response.data.forEach(function(room) {
        if (!room.retorno) {
          // Para cada sala, busca e lista seu paciente
          myApi.getPatient(room.paciente_id).then(
            function(response){
              _listPatients(room, room.criador_id, response);
            },
            function(error) {
              if (error.status === 401) $rootScope.logOut();

              console.log(error);
            });

            i++;
          }
        });

        if (i === 0) $('.list-group-lg').append('<p class="ml-lg mb-md" style="opacity: 0.5">Nenhum agendamento encontrado</p>');

        $scope.loading = false;
    },
    function(error) {
      if (error.status === 401) $rootScope.logOut();

      console.log(error);
    });

  
    myApi.getFinishedAppointments(userId).then(
      function(response) {
        var i = 0;
        // debugger
        // $.each(response.data, function(index, value){
          response.data.forEach(function(room) {
            // Para cada consulta, busca e lista seu paciente
            myApi.getPatient(room.paciente_id).then(
              function(response){
                _listHistory(room, response);
              },
              function(error) {
                if (error.status === 401) $rootScope.logOut();

                console.log(error);
              });

              i++;
          });
        // });

        if (i === 0) $('.list-group-lg').append('<p class="ml-lg mb-md" style="opacity: 0.5">Nenhum agendamento encontrado</p>');

        $scope.loadingH = false;
      },
      function(error) {
        if (error.status === 401) $rootScope.logOut();

        console.log(error);
      });

    function _listPatients(room, owner, patient) {
      if (patient.data) {
        const avatar = patient.data.pacientes[0].foto ? $rootScope.photo + patient.data.pacientes[0].foto : '../img/avatar.png';
        const date = new Date(room.data_prevista);
      
        $scope.pacientes.push({
          id: patient.data.id,
          nome: patient.data.nome,
          email: patient.data.email,
          sala: room.id,
          data_prevista: date,
          avatar: avatar
        });
      }
    };

    function _listHistory(room, patient) {
      if (patient.data) {
        const avatar = patient.data.pacientes[0].foto ? $rootScope.photo + patient.data.pacientes[0].foto : '../img/avatar.png';        
        const date = moment(room.data_prevista).add('3','hours')
        const dateSent = new Date(room.updatedat);
        
        
        $scope.historicos.push({
          id: patient.data.id,
          nome: patient.data.nome,
          email: patient.data.email,
          sala: room.id,
          data_prevista: date,
          data_enviada: dateSent,
          avatar: avatar
        });
      }
    };

    $scope.notify = function(info){
      var notifyPatient = confirm('Deseja notificar o paciente sobre a abertura da sala?');
      var url = '/sala/' + info.sala;

      if (notifyPatient) {
        myApi.sendInvite(info.sala, info.id, info.email).then(
          function(response){
            $location.path(url);
          },
          function(error){
            if (error.status === 401) $rootScope.logOut();

            console.log(error);
          }
        );
      } else {
        $location.path(url);
      }
    };
}]);
