'use strict';

var app = angular.module('myApp.salaVisualizar', []);

app.controller('SalaVisualizarCtrl', ['$scope', '$rootScope', '$routeParams', '$window', 'myApi', 'myCookies', '__env', function($scope, $rootScope, $routeParams, $window, myApi, myCookies, __env) {
  var roomId = $routeParams.room;
  var token = myCookies.getToken();

  $scope.agendamentoApiUrl = __env.agendamentoApiUrl;

  $scope.videos = [];
  $scope.imagens = [];

  $scope.restartVideos = function(){
    for (var i = 0; i < $('video, audio').length; i++) {
      $('video, audio')[i].currentTime = 0;
    }

    $('#btn-start-videos').show();
    $('#btn-pause-videos').hide();
  };

  $scope.startVideos = function(){
    for (var i = 0; i < $('video, audio').length; i++) {
      $('video, audio')[i].play();
    }

    $('#btn-start-videos').hide();
    $('#btn-restart-videos').hide();
    $('#btn-pause-videos').show();
  };

  $scope.pauseVideos = function(){
    for (var i = 0; i < $('video, audio').length; i++) {
      $('video, audio')[i].pause();
    }

    $('#btn-pause-videos').hide();
    $('#btn-start-videos').show();
    $('#btn-restart-videos').show();
  };

  function sortByProperty(property) {
    return function(x, y) {
      return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
    };
  };

  function _pullMessages(messages) {
    $scope.chatLoader = false;
    $scope.mensagens = messages;

    setTimeout(function() {
      $('.post-comments').scrollTop(100000000000000000);
    }, 0);
  };

  // Busca informações da sala atual
  myApi.getRoom(null, roomId).then(
    function(response) {
      var idPatient = response.data.paciente_id;

      // Busca paciente da sala
      myApi.getPatient(idPatient).then(
        function(patient) {
        const avatar = patient.data.pacientes[0].foto ? $rootScope.photo + patient.data.pacientes[0].foto : '../img/avatar.png';
        let date = new Date(patient.data.pacientes[0].data_nascimento);

          date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

          $scope.paciente = {
            id: patient.data.id,
            nome: patient.data.nome,
            sexo: patient.data.pacientes[0].sexo.toUpperCase(),
            data_nasc: date,
            avatar: avatar
          };

          myApi.getPhone(idPatient).then(
            function(patient) {
                // $scope.paciente.celular = patient.data[0] ? patient.data[0].numero : 'Não possui';
                $scope.paciente.telefone = patient.data && patient.data[0] && patient.data[0][0] ? patient.data[0][0].numero : 'Não possui';
            },
            function(error) {
              if (error.status === 401) $rootScope.logOut(roomId);

              console.log(error);
            });
        },
        function(error) {
          if (error.status === 401) $rootScope.logOut(roomId);

          console.log(error);
        });
    },
    function(error) {
      if (error.status === 401) $rootScope.logOut(roomId);

      console.log(error);
    });

  // Busca as mensagens da API e atribui elas ao Scope
  myApi.getMessages(roomId).then(
    function(response) {
      var mensagens = [];
      var itemsProcessed = 0;
      if (response.data.length > 0) {
        response.data.forEach(function(msg) {
          var mensagem = new Object();
          var data = new Date(msg.data_hora_envio);
          var items = response.data.length;

          mensagem.id = msg.id;
          mensagem.local = msg.remetente_id === $rootScope.usuario.id;
          mensagem.chatMessage = false;
          mensagem.mensagem = msg.mensagem;
          mensagem.hora = data.getHours() + ":" + (data.getMinutes() < 10 ? '0' : '') + data.getMinutes();
          mensagem.avatar = '../img/avatar.png';

          myApi.getMessageControl(msg.id).then(
            function(control) {
              if (control.data.length > 0) {
                mensagem.status = control.data[control.data.length - 1].acao === 1 ? 'received' : 'read';
              } else {
                mensagem.status = 'sent';
              }
            },
            function(error) {
              if (error.status === 401) $rootScope.logOut(roomId);

              console.log(error);
            });

            if (msg.prestador) {
                myApi.getUser(token, msg.remetente_id).then(
                    function(response) {
                        mensagem.nome = response.data[0].nome;
                        mensagem.avatar = '../img/avatarMedico.png';

                        mensagens.push(mensagem);

                        itemsProcessed++;

                        if (itemsProcessed == items) {
                            mensagens.sort(sortByProperty('id'));
                            _pullMessages(mensagens);
                        }
                    },
                    function(error) {
                        if (error.status === 401) $rootScope.logOut(roomId);

                        console.log(error);
                    });
            } else {
                myApi.getPatient(msg.remetente_id).then(
                    function(patient) {
                        mensagem.nome = patient.data.nome ? patient.data.nome : 'Anônimo';
                        mensagem.avatar = patient.data.foto ? $rootScope.photo + patient.data.foto : '../img/avatar.png';

                        mensagens.push(mensagem);

                        itemsProcessed++;

                        if (itemsProcessed == items) {
                            mensagens.sort(sortByProperty('id'));
                            _pullMessages(mensagens);
                        }
                    },
                    function(error) {
                        if (error.status === 401) $rootScope.logOut(roomId);

                        console.log(error);
                    });
            }
        });
      } else {
        $scope.chatLoader = false;
      }
    },
    function(error) {
      if (error.status === 401) $rootScope.logOut(roomId);

      console.log(error);
    }
  );

  myApi.getRoomVideos(roomId).then(
    function(response){
      response.data.forEach(function(video){
          $scope.videos.push(video);

          $('#btn-start-videos').show();
      });

      $scope.videos = _removeDuplicates($scope.videos);
    },
    function(error){
      if (error.status === 401) $rootScope.logOut();

      console.log(error);
    });

  myApi.getEditedExams(roomId).then(
    function(response){
      $scope.imagens = response.data;
      $scope.imagens = _removeDuplicates($scope.imagens);
    },
    function(error){
      if (error.status === 401) $rootScope.logOut();

      console.log(error);
    });

    function _removeDuplicates(arr){
      	var newArr = [];

      	angular.forEach(arr, function(value, key) {
      		var exists = false;
      		angular.forEach(newArr, function(val2, key) {
      			if(angular.equals(value.file_id, val2.file_id)){ exists = true };
      		});
      		if(exists == false && value.file_id != "") { newArr.push(value); }
      	});

        return newArr;
    };
}]);
