'use strict';

var app = angular.module('myApp.retornoVisualizar', []);

app.controller('RetornoVisualizarCtrl', ['$scope', '$rootScope', '$routeParams', '$window', 'myApi', '__env', function($scope, $rootScope, $routeParams, $window, myApi, __env) {
  $scope.accessPermission = false; //$routeParams.id;


  myApi.getRoom(null, $routeParams.video).then(
      function(room){
        // Se o usuário for criador do retorno ou paciente, ele tem acesso
        if (room.data.criador_id == $rootScope.usuario.id || $routeParams.id == $rootScope.usuario.id) {
          $scope.accessPermission = true;
          $scope.video = __env.agendamentoApiUrl + '/upload/videos/download/' + $routeParams.video + '.webm';
        }

        // myApi.downloadVideo($routeParams.video).then(
        //     function(video){
        //       // var url = new File();
        //       // console.log(url);
        //       // $scope.video = video;
        //
        //       // var blob = dataURLtoBlob(video.data);
        //       console.log(video);
        //     },
        //     function(error){
        //       console.log(error);
        //     }
        // );
      },
      function(error){
        if (error.status === 401) $rootScope.logOut();

        console.log(error);
      }
  );

  function dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
  }
}]);