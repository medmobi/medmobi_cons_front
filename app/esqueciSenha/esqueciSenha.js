'use strict';

var app = angular.module('myApp.esqueciSenha', ['ngRoute']);

app.controller('EsqueciSenhaCtrl', ['$rootScope', '$scope', '$routeParams', '$window', '$location', 'myApi', 'myCookies', '__env', function($rootScope, $scope, $routeParams, $window, $location, myApi, myCookies, __env) {
  if ($rootScope.err) {
    $scope.error = $rootScope.err;

    $rootScope.err = false;
  } else {
    $scope.error = false;
  }

  $scope.recover = function() {
    myApi.resetPassword($scope.email, __env.consultaOnlineWebUrl + '/#').then(
      function(response) {
        // console.log(response);
        _displaySuccess();
      },
      function(error) {
        console.log(error);
      }
    );
  };

  function _displaySuccess() {
    $scope.error = 'Em breve você receberá um e-mail com o link para recuperação de senha';
    return;
  };
}]);
